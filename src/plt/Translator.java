package plt;

public class Translator {

	public static final String NIL = "nil";
	public static final String NAY = "nay";
	public static final String YAY = "yay";
	public static final String AY = "ay";

	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if(phrase.isEmpty()) return NIL;
		if(phrase.contains(" ")) return translatePhraseWithMatcher(" ");
		else if(phrase.contains("-")) return translatePhraseWithMatcher("-");
		return translateOneWord(phrase);
	}
	
	private String translatePhraseWithMatcher(String matcher) {
		String[] words = phrase.split(matcher);
		StringBuilder finalPhrase = new StringBuilder("");
		for(String word: words) {
			finalPhrase.append(translateOneWord(word) + matcher);
		}
		return finalPhrase.substring(0, finalPhrase.length() - 1);
	}
	
	private String translateOneWord(String word) {
		String finalWord = "";
		boolean containPunctuation = false;
		char punctuation = 0;
		int punctationPosition = -1;
	
		if(containPunctuation(word)) {
			containPunctuation = true;
			punctationPosition = getIndexPunctuation(word);
			punctuation = word.charAt(punctationPosition);
			StringBuilder wordBuilder = new StringBuilder(word);
			wordBuilder.deleteCharAt(punctationPosition);
			word = wordBuilder.toString();
		}
		
		if(startWithVowel(word)) {
			if(word.endsWith("y")) finalWord = word.concat(NAY);
			else if(endWithVowel(word)) finalWord = word.concat(YAY);
			else finalWord = word.concat(AY);
		} else finalWord = translatePhraseStartingWithConsonant(word);
		
		if(containPunctuation)  {
			if(punctationPosition == 0) finalWord = punctuation + finalWord;
			else finalWord = finalWord + punctuation;
		}
		
		return finalWord;
	}
	
	private int getIndexPunctuation(String word) {
		if(word.indexOf('.') != -1) return word.indexOf('.');
		if(word.indexOf(':') != -1) return word.indexOf(':');
		if(word.indexOf(',') != -1) return word.indexOf(',');
		if(word.indexOf(';') != -1) return word.indexOf(';');
		if(word.indexOf('!') != -1) return word.indexOf('!');
		if(word.indexOf('?') != -1) return word.indexOf('?');
		if(word.indexOf(')') != -1) return word.indexOf(')');
		if(word.indexOf('(') != -1) return word.indexOf('(');
		return -1;
	}
	
	private boolean containPunctuation(String word) {
		return (word.contains(".") ||
				word.contains(":") ||
				word.contains(",") ||
				word.contains(";") ||
				word.contains("!") ||
				word.contains("?") ||
				word.contains(")") ||
				word.contains("("));
				
	}
	
	private boolean isVowel(String word, int pos) {
		return (word.charAt(pos) == 'a' ||
				word.charAt(pos) == 'e' ||
				word.charAt(pos) == 'i' ||
				word.charAt(pos) == 'o' ||
				word.charAt(pos) == 'u');
	}
	
	private boolean startWithVowel(String word) {
		return isVowel(word, 0);
	}
	
	private boolean endWithVowel(String word) {
		return isVowel(word, word.length() - 1);
	}
	
	private String translatePhraseStartingWithConsonant(String word) {
		StringBuilder lastPartString = new StringBuilder("");
		int i;
		for(i = 0; i < word.length(); i++) {
			if(isVowel(word, i)) break;
			lastPartString.append(word.charAt(i));
		}
		
		return word.substring(i) + lastPartString.toString() + AY;
	}

}
