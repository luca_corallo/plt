package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	private String helloWorldString = "hello world";
	@Test
	public void testInputPhrase() {
		Translator translator = new Translator(helloWorldString);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyInputPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());		
	}
	
	@Test
	public void testTranslationWordStartingWithMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());		
	}
	
	@Test
	public void testTranslationInputPhraseWithMoreWordsWithSpaces() {
		Translator translator = new Translator(helloWorldString);
		assertEquals("ellohay orldway", translator.translate());		
	}
	
	@Test
	public void testTranslationInputPhraseWithMoreWordsWithDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());		
	}
	
	@Test
	public void testTranslationInputPhraseWithPunctuations() {
		Translator translator = new Translator(helloWorldString + "!");
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationInputPhraseWithMorePunctuations() {
		String inputPhrase = "(well-being)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellway-eingbay)", translator.translate());
	}
	


}
